﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_14
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<Car> Cars { get; set; }


        public override string ToString()
        {
            return $"Id = {Id}; Name = {Name}; Email = {Email}; Phone = {Phone}";
        }
    }
}
