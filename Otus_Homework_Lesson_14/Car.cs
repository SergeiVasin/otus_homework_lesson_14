﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_14
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Person Owner { get; set; }
        public int OwnerId { get; set; }

        public override string ToString()
        {
            return $"Id = {Id}; Name = {Name}; OwnerId = {OwnerId}; OwnerName = {Owner?.Name}";
        }
    }
}
