﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Homework_Lesson_14
{
    public class RandomGenerator
    {
        public static List<Person> GeneratePeople(int dataCount)
        {
            var people = new List<Person>();
            var id = 1;
            var personFaker = new Faker<Person>()
                .CustomInstantiator(f => new Person()
                {
                    Id = id++
                })
                .RuleFor(u => u.Name, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.Name))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            foreach (var p in personFaker.GenerateForever())
            {
                people.Add(p);

                if (dataCount == p.Id)
                    return people;
            }

            return people;
        }

        public static List<Car> GenerateCars(int dataCount)
        {
            var cars = new List<Car>();
            var id = 1;
            var carsFaker = new Faker<Car>()
                .CustomInstantiator(f => new Car()
                {
                    Id = id++
                })
                .RuleFor(c => c.Name, (f, c) => f.Vehicle.Model())
                .RuleFor(c => c.OwnerId, (f, c) => f.Random.Int(1, 100));

            foreach (var c in carsFaker.GenerateForever())
            {
                cars.Add(c);

                if (dataCount == c.Id)
                    return cars;
            }

            return cars;
        }

    }
}
