﻿using Otus_Homework_Lesson_14.CsvSerialization;
using System.Diagnostics;
using System.Reflection;
using System.Text.Json;

namespace Otus_Homework_Lesson_14
{
    class Program
    {
        static void Main()
        {
            DoHomeworkTaskItems_1_8();

            DoHomeworkTaskItems_9_10();
        }

        //п 1- 8 задания
        static void DoHomeworkTaskItems_1_8()
        {


            Console.WriteLine("------------------------------------------------------------------------------------------");
            Console.WriteLine("------------------------------DoHomeworkTaskItems_1-8-------------------------------------");
            Console.WriteLine();

            int test100Iter = 100;
            int test100_000Iter = (int)Math.Pow(10, 5);

            F f = new F { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, MyProp2 = "my property2", MyProp1 = "my property1", MyProp3 = "my property3" };

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("------------------With Serialiser.SerializeToString --------------------");

            Console.WriteLine();
            Console.WriteLine(Serialiser.SerializeToString(f));

            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToString(f); }, test100Iter);
            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToString(f); }, test100_000Iter);

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("---------With Serialiser.SerializeToString and console output-----------");

            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToString(f); Console.Write(""); }, test100Iter);
            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToString(f); Console.Write(""); }, test100_000Iter);

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("---------------With System.Text.Json.JsonSerializer---------------------");

            Console.WriteLine();
            Console.WriteLine(Serialiser.SerializeToJson(f));

            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToJson(f); }, test100Iter);
            //Console.WriteLine();
            //Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToJson(f); }, test100_000Iter);
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("-----With System.Text.Json.JsonSerializer and console output------------");
            Console.WriteLine();
            Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToJson(f); Console.Write(""); }, test100Iter);
           // Console.WriteLine();
            //Benchmark.DoAvgBenchmark(() => { Serialiser.SerializeToJson(f); Console.Write(""); }, test100_000Iter);

        }

        static void DoHomeworkTaskItems_9_10()
        {
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------------------------");
            Console.WriteLine("------------------------------DoHomeworkTaskItems_9-10------------------------------------");
            Console.WriteLine();

            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");

            var objAmount = 1;

            var generatedObjList = RandomGenerator.GeneratePeople(objAmount);

            CsvSerializer.Serialize(";", filePath, generatedObjList);

            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------------------------");
            Console.WriteLine("-------------------Deserialize object from csv file---------------------");
            Console.WriteLine(CsvSerializer.Deserialize<Person>(filePath, ";" ).First());
            Console.WriteLine();
            Benchmark.DoBenchmark(() => { CsvSerializer.Deserialize<Person>(filePath, ";").First(); }, "Deserialization started...", "Deserialization stoped.");
        }

    }

    class F
    {
        public int i1, i2, i3, i4, i5;

        public string MyProp2 { get; set; }
        public string MyProp1 { get; set; }
        public string MyProp3 { get; set; }
    }

    class Serialiser
    {
        public static string SerializeToString(object obj)
        {
            if (obj == null) throw new Exception("Serialization object is null.");


            Type objType = obj.GetType();

            string serString = $"{objType.Name}{{";

            foreach (FieldInfo fi in objType.GetFields())
            {
                serString += fi.Name + " = ";
                serString += fi.GetValue(obj);
                serString += ", ";
            }

            foreach (PropertyInfo pi in objType.GetProperties())
            {
                serString += pi.Name + " = ";
                serString += pi.GetValue(obj);
                serString += ", ";
            }

            serString = serString.Substring(0, serString.Length - 2) + "}";

            return serString;
        }

        public static string SerializeToJson<T>(T obj)
        {
            var options = new JsonSerializerOptions { IncludeFields = true };
            return JsonSerializer.Serialize(obj, options);
        }
    }

    class Benchmark
    {
        public static void DoBenchmark(Action action, string startMes, string endMess)
        {
            Console.WriteLine(startMes);
            var sw = new Stopwatch();
            sw.Start();

            action();

            sw.Stop();
            Console.WriteLine($"{endMess}. Elapsed time: {sw.Elapsed}.");
        }

        public static void DoAvgBenchmark(Action action, int timesNumber)
        {
            TimeSpan[] elapsedTimes = new TimeSpan[timesNumber];


            for (int i = 0; i < timesNumber; i++)
            {
                var sw = new Stopwatch();
                sw.Start();

                action();

                sw.Stop();
                elapsedTimes[i] = sw.Elapsed;
            }


            Console.WriteLine($"Average elapsed time of {timesNumber} iteration times = {elapsedTimes.Average((x) => x.TotalMilliseconds):F4} ms.");            
        }

    }
}